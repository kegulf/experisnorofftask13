﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExperisNoroffTask13.ErrorHandling {
    class EmptyIndexException : Exception {
        public EmptyIndexException() {

            
            // I'm not allowed to edit message, it's read only....

        }

        public EmptyIndexException(string message) : base(message) {
        }

        public EmptyIndexException(string message, Exception innerException) : base(message, innerException) {
        }

        protected EmptyIndexException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }
}
