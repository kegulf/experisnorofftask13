﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExperisNoroffTask13.ErrorHandling {
    class OccupiedIndexException : Exception {
        public OccupiedIndexException() {
        }

        public OccupiedIndexException(string message) : base(message) {
        }

        public OccupiedIndexException(string message, Exception innerException) : base(message, innerException) {
        }

        protected OccupiedIndexException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }
}
