﻿using ExperisNoroffTask13.ErrorHandling;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask13 {
    class ChaosList<T> {

        private T[] items;

        // Amount of items in list
        private int count;

        // Total size of the list
        private int size;

        #region Constructors

        /// <summary>
        ///     Creates a list of size 10.
        /// </summary>
        public ChaosList() {
            size = 10;
            count = 0;

            items = new T[size];
        }

        /// <summary>
        ///     Creates a list of a specified size.
        /// </summary>
        /// <param name="size">The size of the list</param>
        public ChaosList(int size) {
            this.size = size;
            count = 0;

            items = new T[size];
        }

        #endregion



        #region Behaviour

        /// <summary>
        ///     Adds an item to a random index in the list.
        ///     If the random index in the list is occupied,
        ///     the method throws and OccupiedIndexException.
        ///     
        ///     If the list is full it doubles its size.
        /// </summary>
        /// <param name="item">The item to add to the list</param>
        /// <exception cref="OccupiedIndexException"></exception>
        public void Add(T item) {

            if (size == count)
                Resize();

            int index = new Random().Next(0, size);

            if (items[index] != null)
                throw new OccupiedIndexException(
                    $"The random index was occupied, {item} not added.."
                );

            items[index] = item;
            count++;
        }



        /// <summary>
        ///     Generates a random index and removes and returns the item on the index from the list. 
        ///     If the item at the index is null, it throws a EmptyIndexException.
        /// </summary>
        /// <returns>The removed item</returns>
        /// <exception cref="EmptyIndexException"></exception>
        public T Remove() {
            int index = new Random().Next(0, size);

            if (items[index] == null)
                throw new EmptyIndexException(
                    "Tried to remove an item from an unoccupied index.. No item removed"
                );

            T itemToReturn = items[index];

            // Reset the item at the index to the default value of the type of T
            // T is not necesserally nullable.
            items[index] = default(T);
            count--;

            return itemToReturn;
        }



        /// <summary>
        ///     Doubles the size of the list.
        /// </summary>
        public void Resize() {
          
            size *= 2;
            Array.Resize(ref items, size); 
        }


        /// <summary>
        ///     Checks if the count is equal to 0
        /// </summary>
        /// <returns>true is count is 0, else false</returns>
        public bool IsEmpty() {
            return count == 0;
        }

        #endregion



        #region Overrides/Implements

        public override string ToString() {
            
            string returnString = "[";

            for (int i = 0; i < size; i++) {

                returnString += (items[i] == null) ? "null" : items[i].ToString();
                returnString += (i == size - 1) ? "" : ", ";
            }

            return $"{returnString}]";
        }

        #endregion


        #region Accessor Methods

        /// <summary>
        ///     Amount of elements in the list
        /// </summary>
        public int Count { get => count; }

        /// <summary>
        /// Total size of the array
        /// </summary>
        public int Size { get => size; }

        #endregion



    }
}
