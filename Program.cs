﻿using ExperisNoroffTask13.ErrorHandling;
using System;

namespace ExperisNoroffTask13 {
    class Program {
        static void Main(string[] args) {

            ChaosList<string> chaoticStrings = new ChaosList<string>();

            TestAddingToList(chaoticStrings);

            TestRemovalFromList(chaoticStrings);
        }



        private static void TestRemovalFromList(ChaosList<string> chaoticStrings) {
            int attempts = 0;

            while(attempts < 1000 && !chaoticStrings.IsEmpty()) {

                try {
                    string removedItem = chaoticStrings.Remove();
                    Console.WriteLine($"{removedItem} was removed"); 
                }

                catch (EmptyIndexException eie) {
                    Console.WriteLine(eie.Message);
                }

            }

            Console.WriteLine("\nList is now empty, hopefully O:)\n");

            Console.WriteLine(chaoticStrings);
        }



        static void TestAddingToList(ChaosList<string> chaoticStrings) {
            // I WILL FILL THIS LIST, IF IT SO TAKES ME A 1000 TRIES!
            int attempts = 0;
            while (attempts < 1000 && chaoticStrings.Count < 10) {

                try {
                    chaoticStrings.Add($"Attempt {++attempts}");
                    Console.WriteLine($"Attempt {attempts} was added to the list");
                }

                catch (OccupiedIndexException oie) {
                    Console.WriteLine(oie.Message);
                }

            }

            Console.WriteLine("\nList is hopefully full by now");
            // List is now full, default size is 10
            Console.WriteLine($"Count of list: {chaoticStrings.Count}\n" +
                $"Size of list: {chaoticStrings.Size}\n" +
                $"{chaoticStrings}\n");
            

            Console.WriteLine("Trying to add another item");
            // This should resize the list
            try {
                chaoticStrings.Add("Testing");
            }

            catch (OccupiedIndexException oie) {
                Console.WriteLine(oie.Message);
            }

            Console.WriteLine("\nList should now be resized");
            Console.WriteLine($"Count of list: {chaoticStrings.Count}\n" +
                $"Size of list: {chaoticStrings.Size}\n" +
                $"{chaoticStrings}\n");

        }
    }
}
